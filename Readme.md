Hello, here is a simple tutorial how to use this json dump to evaluate rarities of punks.

I've decided to share only the dump itself since sharing code could be considered unsafe.
Tho this is a bit cumbersome i wanted to share this as soon as possible

All you have to do is go to this json online editor (https://jsonformatter.org/json-editor) where you can load data from this URL: https://gitlab.com/prizes-shillverse-projects/rarity-tool/-/raw/main/json_dump.json and then filter it as you like.


Total_score TRYIES to estimate rarity of punks.

Also rarities will change with more punks minted.

Rarity tool only considers: number of accessories,accessories,race. Typos and stats are not included in rarity score.

There are multiple ways to determine rarity this is just one of them.


Some tips:

    1. Best viewing experience is on right side of screen where you can collapse and expand fields
    2. I would suggest first collapse all the fields using second button in the right editor then you can expand what ever you like and it should be easier to read
    3. Funnel shaped button can be used for filtering desired punks 
    4. Most useful filtering will be: filter by id field
                                       compare with either == (equal) or > (greater than) (for instance you want to know rarity of last 10 punks added)
                                       lastly value will be desired id by which you want to filer
    5. You can revert filtering by pressing undo button

If you are feeling brave you can just download json_dump.json file from this repository and play with it your self. Tho if you dont know if its safe you are better using method above.

Note: json file is already pre sorted based on total_score value
